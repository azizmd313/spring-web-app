package com.example.demo.dao;

import com.example.demo.model.Person;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface PersonDao {

    public int insertPerson(UUID uuid, Person person);

    default int insertPerson(Person person){
        UUID uuid = UUID.randomUUID();
        return insertPerson(uuid,person);
    }

    public List<Person> getPeople();

    public Boolean deletePerson(UUID uuid);

    public Boolean updatePerson(UUID uuid, Person person);

    public Optional<Person> selectPerson(UUID uuid);
}
