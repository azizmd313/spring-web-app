package com.example.demo.dao;

import com.example.demo.model.Person;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository("fakeDao")
public class FakePersonDataAccessService implements PersonDao{
    ArrayList<Person> DB = new ArrayList<>();

    @Override
    public int insertPerson(UUID uuid, Person person) {
        DB.add(new Person(uuid,person.getName()));
        return 1;
    }

    @Override
    public List<Person> getPeople() {
        return DB;
    }

    @Override
    public Boolean deletePerson(UUID uuid) {
        Optional<Person> person = selectPerson(uuid);

        if(person.isPresent()){
            DB.remove(person.get());
            return true;
        }
        return false;
    }

    @Override
    public Boolean updatePerson(UUID uuid, Person person) {
        selectPerson(uuid).map(p->{
           int indexToUpdate = DB.indexOf(p);
           if(indexToUpdate >=0){
               Person tmpPerson = new Person(uuid,person.getName());
               DB.set(indexToUpdate,tmpPerson);
               return true;
           }
           else{
               return false;
           }
        });

        return false;
    }

    @Override
    public Optional<Person> selectPerson(UUID uuid) {
        return DB.stream()
                .filter(person -> person.getId().equals(uuid))
                .findFirst();
    }
}
